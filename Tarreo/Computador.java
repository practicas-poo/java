

package tarreo;


public class Computador {
    private String nombre;
    private String tarjeta;
    private int memoria;

    public Computador(String nombre, String tarjeta, int memoria) {
        this.nombre = nombre;
        this.tarjeta = tarjeta;
        this.memoria = memoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public int getMemoria() {
        return memoria;
    }
    
    
    
    public void setTarjeta(String tarjeta){
        if(tarjeta.length()>=2){
            this.tarjeta = tarjeta;
        }else{
            System.out.println("El nombre de la tarjeta no debe ser menor a 2 caracteres");
        }
    }
    
    public void setMemoria(int memoria){
        if(memoria > 0){
            this.memoria = memoria;
        }else{
            System.out.println("La memoria no debe ser menor a 0.");
        }
    }
    
}
