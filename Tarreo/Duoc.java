

package tarreo;


public class Duoc {
    private Persona[] participantes;
    private int cantidadParticipantes;
    private int capacidadMaxima;

    public Duoc(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
        this.participantes = new Persona[capacidadMaxima];
    }

    public Persona[] getParticipantes() {
        return participantes;
    }

    public void setParticipantes(Persona[] participantes) {
        this.participantes = participantes;
    }

    public int getCantidadParticipantes() {
        return cantidadParticipantes;
    }

    public void setCantidadParticipantes(int cantidadParticipantes) {
        this.cantidadParticipantes = cantidadParticipantes;
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }
    
    public void agregar(Persona nuevaPersona, int indice){
        participantes[indice] = nuevaPersona;
    }
    public boolean buscar(String rut){
        boolean valida = false;
        for(int i = 0; i < participantes.length;i++){
            if(participantes[i].getRut().equals(rut)){
                valida = true; 
                break;
            }
        }
        return valida;
    }
    
    public void listar(){
        System.out.println("----------- INFORME -------------");
        for(Persona tmpPersona : participantes){
            System.out.println("Computador: " + tmpPersona.getComputador().getNombre());
            System.out.println("Username: " + tmpPersona.getUsername());
            System.out.println("Edad: " + tmpPersona.getEdad());
        }
        System.out.println("---------------------------------");
    }
    
    public Persona[] obtenerNoobHombres(){
        Persona[] listado = new Persona[capacidadMaxima];
        int ubicacion = 0;
        for(int i = 0; i < participantes.length; i++){
            if(participantes[i].isExperiencia() == false && participantes[i].getSexo() == 'M'){
                listado[ubicacion] = participantes[i];
                ubicacion ++;
            }
        }
        return listado;
    }
}
