

package tarreo;


public class Persona {
    private String rut;
    private String username;
    private int edad;
    private char sexo;
    private boolean experiencia;
    private Computador computador;

    public Persona(String rut, String username, int edad, char sexo, boolean experiencia, Computador computador) {
        this.rut = rut;
        this.username = username;
        this.edad = edad;
        this.sexo = sexo;
        this.experiencia = experiencia;
        this.computador = computador;
    }
    
    
    
    public void setEdad(int edad){
        if(edad >= 18 && edad <= 30){
            this.edad = edad;
        }else{
            System.out.println("Edad debe ser entre 18 y 30 años");
        }
    }
    
    public void setSexo(char sexo){
        if(sexo == 'M' || sexo == 'F'){
            this.sexo = sexo;
        }else{
            System.out.println("Sexo debe ser F o M.");
        }
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isExperiencia() {
        return experiencia;
    }

    public void setExperiencia(boolean experiencia) {
        this.experiencia = experiencia;
    }

    public Computador getComputador() {
        return computador;
    }

    public void setComputador(Computador computador) {
        this.computador = computador;
    }

    public int getEdad() {
        return edad;
    }

    public char getSexo() {
        return sexo;
    }
    
    public String imprimir(){
        String info = "";
        info += "Rut: " + rut + "\n";
        info += "Nombre de usuario: " + username + "\n";
        info += "Edad: " + edad + "\n";
        info += "Genero: " + sexo;
        info += "Experiencia: " + experiencia + "\n";
        return  info;
    }
}
