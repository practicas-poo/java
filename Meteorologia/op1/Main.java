

package meteorologia.op1;

import java.util.Scanner;

public class Main {

 
    public static void main(String[] args) {
        Temperatura temperatura;
        int cantidad, temperaturasNegativas;
        double valorTemperatura, sumaTemperaturas;
        double mayorTemperatura, menorTemperatura;
        Scanner leer = new Scanner(System.in);
        
        sumaTemperaturas = 0;
        temperaturasNegativas = 0;
        mayorTemperatura = -100;
        menorTemperatura = 100;
        
        System.out.print("Cuentas temperaturas desea agregar?");
        cantidad = leer.nextInt();
        System.out.println("");
        
        temperatura = new Temperatura(cantidad);
        
        //Llenar arreglo temperatura
        for (int i = 0; i < cantidad; i++) {
            System.out.print("Ingrese temperatura " + (i + 1) + ": ");
            valorTemperatura = leer.nextDouble();
            temperatura.agregarTemperatura(valorTemperatura);
        }
        
        
        
        for (int i = 0; i < cantidad; i++) {
            valorTemperatura = temperatura.devolverTemperatura(i);
            
            //Obtener suma de temperaturas
            sumaTemperaturas +=  valorTemperatura;
            
            //Obtener temperatura maxima
            if(valorTemperatura > mayorTemperatura){
                mayorTemperatura = valorTemperatura;
            }
            
            //Obtener temperatura minima
            if(valorTemperatura < menorTemperatura){
                menorTemperatura = valorTemperatura;
            }
            
            //Obtener cantidad de temperaturas bajo 0°C
            if(valorTemperatura < 0){
                temperaturasNegativas += 1;
            }
        }
        
        //Mostrar resultados
        System.out.println("El promedio de temperatura es: " + (sumaTemperaturas / cantidad));
        System.out.println("La mayor temperatura es: " + mayorTemperatura);
        System.out.println("La menor temperatura es: " + menorTemperatura);
        System.out.println("La cantidad de temperaturas bajo 0°C son: " + temperaturasNegativas);
        System.out.println(temperatura.imprimirTemperatura());
    }
}
