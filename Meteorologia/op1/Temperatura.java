package meteorologia.op1;

public class Temperatura {
    
    double[]  temperaturas;
    int posicion = 0;
    
    public Temperatura(int largo) {
        temperaturas = new double[largo];
    }
    
    public void agregarTemperatura(double temperatura){
        temperaturas[posicion] = temperatura;
        posicion = posicion + 1;
    }
    
    public double devolverTemperatura(int posicion){
        return temperaturas[posicion];
    }
    
    public String imprimirTemperatura(){
    
        String informe = "";
        for (int i =0; i< temperaturas.length; i++){
            informe += "Temperatura n "+ (i+1) + " : " + temperaturas[i] + "\n";
        }
        return informe;
    }
}
    
    

