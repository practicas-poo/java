package meteorologia.op2;

public class Temperatura {
    
    private double[] temperaturas;

    public Temperatura() {
    }

    public double[] getTemperatura() {
        return temperaturas;
    }

    public void setTemperatura(double[] temperatura) {
        this.temperaturas = temperatura;
    }
    
    public void crearTemperaturas(int cantidad){
        temperaturas = new double[cantidad];
    }
    
    public void agregarTemparaturas(int posicion, double temperatura){
        temperaturas[posicion] = temperatura;
    }
    
    public double temperaturaMasAlta(){
        double temperaturaMayor = temperaturas[0];
        for (int i = 0; i < temperaturas.length; i++) {
            if(temperaturas[i]>temperaturaMayor){
                temperaturaMayor = temperaturas[i];
            }
        }
        return temperaturaMayor;
    }
    
    public double temperaturaMasBaja(){
        double temperaturaMenor = temperaturas[0];
        for (int i = 0; i < temperaturas.length; i++) {
            if(temperaturas[i]<temperaturaMenor){
                temperaturaMenor = temperaturas[i];
            }
        }
        return temperaturaMenor;
    }
    
    public double temperaturaPromedio(){
        double sumaTemperaturas = 0;
        for (int i = 0; i < temperaturas.length; i++) {
            sumaTemperaturas += temperaturas[i];
        }
        return sumaTemperaturas / temperaturas.length;
    }
    
    public int cantidadTemperaturaBajoCero(){
        int contador = 0;
        double temperatura = temperaturas[0];
        for (int i = 0; i < temperaturas.length; i++) {
            if(temperaturas[i]<0){
                contador += 1;
            }
        }        
        return contador;    
    }
    
    public String imprimirTemperaturas(){
        String info = "";
        for (int i = 0; i < temperaturas.length; i++) {
            info += "Temparatura n° " + i + ": " + temperaturas[i] + "\n";
        }
        return info;
    }
}
