package meteorologia.op2;

import java.util.Scanner;

public class Main {
    
    public static void main(String[] args) {
        
        Scanner leer = new Scanner(System.in);
        Temperatura t = new Temperatura();
        int opcion, cantidad = 0;
        double temperatura;
        
        do {
            System.out.println("*************** Menu ***************");
            System.out.println("1.- Agregar temperaturas");
            System.out.println("2.- Ver temperatura más alta");
            System.out.println("3.- Ver temperatura más baja");
            System.out.println("4.- Ver promedio temperaturas");
            System.out.println("5.- Ver Cantidad temperaturas > 0°C");
            System.out.println("6.- Imprimir temperaturas");
            System.out.println("7.- Salir");
            System.out.println("************************************");
            System.out.print("Ingrese la opción requerida: ");
            opcion = leer.nextInt();
            
            switch(opcion){
                case 1: 
                    do{
                        System.out.print("Ingrese la cantidad de temperaturas: ");
                        cantidad = leer.nextInt();
                        t.crearTemperaturas(cantidad);
                        for (int i = 0; i < cantidad; i++) {
                            System.out.print("Ingrese temparatura n° " + (i+1) + ": ");
                            temperatura = leer.nextDouble();
                            t.agregarTemparaturas(i, temperatura);
                        }
                        System.out.println("************************************");
                        System.out.print("Presione (0) para volver: ");
                        opcion = leer.nextInt();
                    } while(opcion != 0);
                    break;
                case 2: 
                    System.out.println("************************************");
                    System.out.println(t.temperaturaMasAlta());
                    System.out.println("************************************");
                    break;
                case 3: 
                    System.out.println("************************************");
                    System.out.println(t.temperaturaMasBaja());
                    System.out.println("************************************");
                    break;
                case 4: 
                    System.out.println("************************************");
                    System.out.println(t.temperaturaPromedio());
                    System.out.println("************************************");
                    break;
                case 5: 
                    System.out.println("************************************");
                    System.out.println(t.cantidadTemperaturaBajoCero());
                    System.out.println("************************************");
                    break;
                case 6:
                    System.out.println("************************************");
                    System.out.println(t.imprimirTemperaturas());
                    System.out.println("************************************");
                    break;
                case 7: 
                    System.out.println("************************************");
                    System.out.println("FIN DEL PROGRAMA DE METEOROLOGIA");
                    System.out.println("************************************");
                    break;    
            }
                
        } while(opcion != 7);
    }
}
