package listas;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        String[] nombres = new String[3];
        int[] numeros = new int[3];
        numeros[0] = 100;
        numeros[1] = 50;
        
        nombres[0] = "Juan";
        nombres[1] = "Moises";
        nombres[2] = "David";
        
        System.out.println("=> Recorrer arreglo");
        for (int i = 0; i < nombres.length; i++) {
            System.out.println("["+i+"]"+"Nombre: " + nombres[i]);
        }
        
        System.out.println("=>Largo arreglo nombres: " + nombres.length);
        
        System.out.println("=>Editar ubicación arreglo");
        nombres[2] = "Pedro";
        for (int i = 0; i < nombres.length; i++) {
            System.out.println("["+i+"]"+"Nombre: " + nombres[i]);
        }
        
        System.out.println("=>Eliminar valor de una ubicación arreglo");
        nombres[2] = "";
        for (int i = 0; i < nombres.length; i++) {
            System.out.println("["+i+"]"+"Nombre: " + nombres[i]);
        }
        
        System.out.println("=>Cargar automatica un arreglo");
          for (int i = 0; i < nombres.length; i++) {
            nombres[i] = "Prefijo_"+i;
            System.out.println(nombres[i]);
        } 
          
        System.out.println("=> Utilizando colecciones : ArrayList");  
        ArrayList<String> autos = new ArrayList<String>();
        autos.add("Honda");
        autos.add("Nissan");
        autos.add("Toyota");
        autos.add("Cherry");
        autos.add("BMV");
        
        for (int i = 0; i < autos.size(); i++) {
            System.out.println("=> " + autos.get(i).toUpperCase());
        }
        
        autos.set(0, "Lada");
        
        for (int i = 0; i < autos.size(); i++) {
            System.out.println("=> " + autos.get(i).toUpperCase());
        }
        
        for (int i = 0; i < autos.size(); i++) {
            if(autos.get(i).equals("Lada")){
                autos.remove(i);
            }
        }
        
        for (int i = 0; i < autos.size(); i++) {
            System.out.println("=> " + autos.get(i));
        }
       
        autos.clear();
        
        if(autos.size() == 0){
            System.out.println("Autos se encuentra sin valores");
        }
        
        String numeroUno = "1";
        int uno = Integer.parseInt(numeroUno);
        String letra = Integer.toString(uno);
    }
    
}
