package discografia;

public class Main {

    public static void main(String[] args) {
        Persona interprete;
        Cancion cancion01, cancion02;
        Album album;
        
        interprete = new Persona("Madonna", 50, 'F');
        cancion01 = new Cancion("Vogue", 4, true, interprete);
        cancion02 = new Cancion("Like a Virgen", 3, false, interprete);
        
        album = new Album();
        
        album.agregarCancion(cancion01);
        album.agregarCancion(cancion02);
        
        album.listarCanciones();
        
        album.eliminarCancion("Vogue");
        
        album.listarCanciones();
    }
}
