package discografia;

public class Persona {
    private String nombre;
    private int edad;
    private char genero;

    public Persona(String nombre, int edad, char genero) {
        setNombre(nombre);
        setEdad(edad);
        setGenero(genero);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        if(edad >= 5 && edad <= 90){
            this.edad = edad;
        } else {
            System.out.println("La edad debe estar en el rango 5 - 90 incluidos");
        }
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        if(genero == 'F' || genero == 'M'){
            this.genero = genero;
        } else {
            System.out.println("El genero debe ser F ó M");
        }
    }
    
    
}
