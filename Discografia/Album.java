package discografia;

import java.util.ArrayList;

public class Album {
    ArrayList<Cancion> canciones;

    public Album() {
        canciones = new ArrayList<Cancion>();
    }
    
    public void agregarCancion(Cancion nuevaCancion){
        if(verificarCancion(nuevaCancion.getNombre(), nuevaCancion.getInterprete().getNombre()) == false){
            canciones.add(nuevaCancion);
        }else{
            System.out.println("Canción existe");
        }
    }
    
    public boolean verificarCancion(String cancion, String interprete){
        boolean valida = false;
        for(Cancion tmpCanciones : canciones){
            if(tmpCanciones.getNombre().equals(cancion) && tmpCanciones.getInterprete().getNombre().equals(interprete)){
                valida = true;
                break;
            }
        }
        return valida;
    }
    
    public void listarCanciones(){
        for (Cancion tmpCancion : canciones) {
            tmpCancion.imprimirCancion();
        }
    }
    
    public void eliminarCancion(String nombreCancion){
        for(int i = 0; i < canciones.size(); i++){
            if(canciones.get(i).getNombre().equals(nombreCancion)){
                canciones.remove(i);
                i = i - 1;
            }
        }
    }
}
