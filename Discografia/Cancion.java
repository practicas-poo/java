package discografia;

public class Cancion {
    private String nombre;
    private int duracion;
    private boolean enVivo;
    private Persona interprete;

    public Cancion(String nombre, int duracion, boolean enVivo, Persona interprete) {
        setNombre(nombre);
        setDuracion(duracion);
        setEnVivo(enVivo);
        setInterprete(interprete);
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if(nombre.length() >= 2){
            this.nombre = nombre;
        } else {
            System.out.println("El nombre debe tener minimo 2 caracteres");
        }
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        if(duracion > 1){
            this.duracion = duracion;
        } else {
            System.out.println("La duración debe ser mayor a 1 minuto");
        }
    }

    public boolean isEnVivo() {
        return enVivo;
    }

    public void setEnVivo(boolean enVivo) {
        this.enVivo = enVivo;
    }

    public Persona getInterprete() {
        return interprete;
    }

    public void setInterprete(Persona interprete) {
        this.interprete = interprete;
    }
    
    public void imprimirCancion(){        
        System.out.println("--------- Canción ---------");
        System.out.println("Nombre: " + nombre);
        System.out.println("Duración: " + duracion);
        System.out.println("En Vivo: " + enVivo);
        System.out.println("---------------------------");
        System.out.println("-------- Interprete -------");
        System.out.println("Nombre: " + interprete.getNombre());
        System.out.println("Edad: " + interprete.getEdad());
        System.out.println("Genero: " + interprete.getGenero());
        System.out.println("---------------------------");
    }
    
    public String concatenar(){
        return nombre + " - " + interprete.getNombre();
    }
    
    public void mostrarCategoria(){
        String categoria = "";
        if(duracion < 30){
            categoria = "Corta";
        } else if (duracion >= 30 && duracion <= 200){
            categoria = "Media";
        } else {
            categoria = "Larga";
        }
        System.out.println("Categoria: " + categoria);
    }
    
}
