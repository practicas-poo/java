package arreglos;

import auxiliar.Persona;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        Persona p = new Persona();
        
        System.out.print("Ingrese su nombre: ");
        p.setNombre(leer.nextLine());
        
        System.out.print("Ingrese su año nacimiento: ");
        p.setAñoNacimiento(leer.nextInt());
        
        System.out.println("Seleccione su genero");
        System.out.print(p.opcionesGenero() + ":");
        int i = leer.nextInt();
        
        System.out.println("--------------------------");
        System.out.println(p.detallePersona(i));
    }
    
}
