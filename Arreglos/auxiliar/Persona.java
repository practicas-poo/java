package auxiliar;

public class Persona {
    private String nombre;
    private int añoNacimiento;
    private String[] genero;

    public Persona() {
        genero = new String[3];
        genero[0] = "Femenino";
        genero[1] = "Masculino";
        genero[2] = "Otros";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAñoNacimiento() {
        return añoNacimiento;
    }

    public void setAñoNacimiento(int añoNacimiento) {
        this.añoNacimiento = añoNacimiento;
    }
    

    public String[] getGenero() {
        return genero;
    }

    public void setGenero(String[] genero) {
        this.genero = genero;
    }
    
    public String detallePersona(int i){
        String informe = "";
        informe += "Nombre : " + this.nombre + "\n";
        informe += "Edad   : " + calculaEdad() + "\n";
        informe += "Genero : " + this.genero[i];
        return informe;
    }
    
    private int calculaEdad(){
        return 2014 - añoNacimiento;
    }
    
    public String opcionesGenero(){
        String opciones = "";
        for (int i = 0; i < genero.length; i++) {
            opciones += "(" + i + ") " + genero[i] + "\n";
        }
        return opciones;
    }
}
