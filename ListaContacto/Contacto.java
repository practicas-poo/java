
package listacontacto;

import java.util.ArrayList;


public class Contacto {
    private ArrayList<Persona> listaPersonas;

    // Costructor de la clase
    public Contacto() { 
        listaPersonas = new ArrayList<Persona>();
    }
  
    // Getters & setters
    public ArrayList<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(ArrayList<Persona> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }
    
    private boolean verificarNumero(String numero)
    {
       boolean valida = false;
       for(Persona tmpPersona : listaPersonas){
           if (tmpPersona.getCelular().equals(numero))
           {
               valida = true;
               break;
           }
       }
       return valida;
    }
     
    
    public void agregarContacto(Persona nuevaPersona)
    {
        if(!verificarNumero(nuevaPersona.getCelular())){
            listaPersonas.add(nuevaPersona);
        }
        else{
            System.out.println("Estimado favor antes de agregar un contacto ingresar celular");
        }
    }
    
    public void listarContactos()
    {
        for(Persona tmpPersona: listaPersonas)
        {
            System.out.println("==== INFO CONTACTO ====");
            System.out.println("n° Celular: " + tmpPersona.getCelular());
            System.out.println("Nombre: " + tmpPersona.getNombre());
            System.out.println("Direccion: " + tmpPersona.getDireccion());
            System.out.println("Correo Electronico: " + tmpPersona.getCorreo());
            System.out.println("==============");
        }
    }
    
    public void eliminarLista(String parametro){
        for (int i = 0; i < listaPersonas.size(); i++) {
            if(listaPersonas.get(i).getCelular().equals(parametro)){
                listaPersonas.remove(i);
                i-=1;
            }
        }
    }
    
    public int obtenerListaContactos(String letra)
    {
        int contador = 0;
        for(Persona tmpPersona : listaPersonas){
            if(tmpPersona.getNombre().substring(0,0).equals(letra)){
                contador++;
            }
        }
        return contador;
    }
}
