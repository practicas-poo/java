
package listacontacto;

public class Persona {
    private String celular;
    private String nombre;
    private String direccion;
    private String correo;

    
     // Costructor de la clase
    public Persona() {
    }
    
    // Getters & setters
    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public void imprimir(){
        System.out.println("n° Celular: " + celular);
        System.out.println("Nombre: " + nombre);
        System.out.println("Direccion: " + direccion);
        System.out.println("Correo Electronico: " + correo);
        
    
    }
            
    
}
