import java.util.Scanner;
public class Main
{
    public static void main(String arg[])
    {
        Scanner leer = new Scanner(System.in);
        Puerta puerta = new Puerta();
        
        String color = "";
        System.out.print('\u000C');
        System.out.print("Favor ingresar el color de la puerta: ");
        color = leer.nextLine();
        puerta.setColor(color);
        
        double alto = 0;
        System.out.print("Favor ingresar el alto de la puerta: ");
        alto = leer.nextDouble();
        puerta.setAlto(alto);
        
        double ancho = 0;
        System.out.print("Favor ingresar el ancho de la puerta: ");
        ancho = leer.nextDouble();
        puerta.setAncho(ancho);
        
        System.out.println("-------------------------------------");
        puerta.abrirPuerta();
        System.out.println(puerta.detallePuerta());
    }
}
