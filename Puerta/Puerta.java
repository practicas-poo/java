public class Puerta
{
    private String _color;
    private double _alto;
    private double _ancho;
    private boolean _estadoPuerta;
    
    public Puerta()
    {
        this._color = "";
        this._alto = 0;
        this._ancho = 0;
        this._estadoPuerta = false;
    }
    
    public String getColor(){return this._color;}
    public void setColor(String color){this._color = color;}
    
    public double getAlto(){return this._alto;}
    public void setAlto(double alto){this._alto = alto;}
    
    public double getAncho(){return this._ancho;}
    public void setAncho(double ancho){this._ancho = ancho;}
    
    public boolean getEstadoPuerta() {return this._estadoPuerta;}
    public void setEstadoPuerta(boolean estadoPuerta) {this._estadoPuerta = estadoPuerta;}
    
    public void abrirPuerta ()
    {
        if(getEstadoPuerta()==false){
            setEstadoPuerta(true);
        }
        else {
            setEstadoPuerta(false);
        }
    }
    
    public String detallePuerta(){
        String informacion = "";
        informacion += "Color: " + getColor() + "\n";
        informacion += "Alto: " + getAlto() + "\n";
        informacion += "Ancho: " + getAncho() + "\n";
        informacion += "Estado: " + getEstadoPuerta();
        
        return informacion;
    }
}
